<?php

declare(strict_types=1);

namespace Drupal\Tests\language_country_negotiation\Functional;

use Drupal\Tests\system\Functional\Module\GenericModuleTestBase;

/**
 * Generic module test for language country negotiation.
 *
 * @group language_country_negotiation
 */
class GenericTest extends GenericModuleTestBase {

  /**
   * {@inheritdoc}
   */
  protected function preUninstallSteps(): void {
    $storage = \Drupal::entityTypeManager()->getStorage('lcn_country');
    $countries = $storage->loadMultiple();
    $storage->delete($countries);
  }

}
