<?php

declare(strict_types=1);

namespace Drupal\Tests\language_country_negotiation\Kernel;

use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\language\Entity\ConfigurableLanguage;

/**
 * Test base for country services.
 */
abstract class CountryTestBase extends EntityKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'language',
    'language_country_negotiation',
  ];

  /**
   * Some example countries data.
   *
   * @var array
   */
  protected array $exampleCountries = [
    'germany' => [
      'name' => 'Germany',
      'country_code' => 'de',
      'available_languages' => ['de'],
      'status' => 1,
    ],
    'belgium' => [
      'name' => 'Belgium',
      'country_code' => 'be',
      'available_languages' => ['nl', 'fr', 'de'],
      'status' => 1,
    ],
    'usa' => [
      'name' => 'United States of America',
      'country_code' => 'us',
      'available_languages' => ['en'],
      'status' => 1,
    ],
    'south-korea' => [
      'name' => 'South Korea',
      'country_code' => 'kr',
      'available_languages' => ['en'],
      'status' => 1,
    ],
    'china' => [
      'name' => 'China',
      'country_code' => 'cn',
      'available_languages' => ['zh-hans'],
      'status' => 0,
    ],
    'egypt' => [
      'name' => 'Egypt',
      'country_code' => 'eg',
      'available_languages' => ['en'],
      'status' => 0,
    ],
    'morocco' => [
      'name' => 'Morocco',
      'country_code' => 'ma',
      'available_languages' => ['fr'],
      'status' => 1,
    ],
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->installConfig([
      'language_country_negotiation',
      'language',
    ]);
    $this->installEntitySchema('lcn_country');

    // Add additional language for translation. English default language.
    ConfigurableLanguage::createFromLangcode('de')->save();
    ConfigurableLanguage::createFromLangcode('fr')->save();
    ConfigurableLanguage::createFromLangcode('nl')->save();
    ConfigurableLanguage::createFromLangcode('zh-hans')->save();
  }

  /**
   * Sets up the country entities.
   *
   * @param array $countries
   *   The country entity values.
   */
  protected function setUpCountries(array $countries): void {
    $storage = $this->entityTypeManager->getStorage('lcn_country');
    foreach ($countries as $country) {
      $storage->save($storage->create($country));
    }
  }

}
