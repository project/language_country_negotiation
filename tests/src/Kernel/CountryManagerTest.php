<?php

declare(strict_types=1);

namespace Drupal\Tests\language_country_negotiation\Kernel;

use Drupal\language_country_negotiation\Service\CountryManagerInterface;
use Drupal\language_country_negotiation\Service\CurrentCountry;

/**
 * Tests the language_country_negotiation.country_manager service.
 *
 * @coversDefaultClass \Drupal\language_country_negotiation\Service\CountryManager
 * @group language_country_negotiation
 */
class CountryManagerTest extends CountryTestBase {

  /**
   * The language_country_negotiation.country_manager service.
   *
   * @var \Drupal\language_country_negotiation\Service\CountryManagerInterface
   */
  protected CountryManagerInterface $countryManager;

  /**
   * The language_country_negotiation.current_country service.
   *
   * @var \Drupal\language_country_negotiation\Service\CurrentCountry
   */
  protected CurrentCountry $currentCountry;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->countryManager = $this->container->get('language_country_negotiation.country_manager');
    $this->currentCountry = $this->container->get('language_country_negotiation.current_country');
  }

  /**
   * Tests the get current country code proxy method.
   *
   * @covers ::getCurrentCountryCode
   */
  public function testGetCurrentCountryCode(): void {

    $this->assertNull($this->countryManager->getCurrentCountryCode(), 'Service returns null because the country is not set.');

    $this->currentCountry->setCountryCode('fr');
    $this->assertEquals('fr', $this->countryManager->getCurrentCountryCode(), 'Service returns the country code proxied from the current country service.');

    $this->currentCountry->setCountryCode('de');
    $this->assertEquals('de', $this->currentCountry->getCurrentCountryCode(), 'Service returns the consecutively set country code proxied from the current country service.');

    $this->currentCountry->resetCountryCode();
    $this->assertNull($this->currentCountry->getCurrentCountryCode(), 'Service returns NULL because the current country was reset.');
  }

  /**
   * Tests checking if a country is allowed.
   *
   * @param array $countries
   *   The country data as expected by ::setUpCountries().
   * @param string|null $country_code
   *   The country code to check.
   * @param bool $allowed
   *   The expected result.
   *
   * @covers ::isCountryAllowed
   * @dataProvider isCountryAllowedProvider
   */
  public function testIsCountryAllowed(array $countries, ?string $country_code, bool $allowed): void {
    $this->setUpCountries($countries);
    $this->assertEquals($allowed, $this->countryManager->isCountryAllowed($country_code));
  }

  /**
   * Data provider for testIsCountryAllowed.
   *
   * @return array
   *   A list of test scenarios.
   */
  public function isCountryAllowedProvider(): array {
    $cases['disallowed'] = [
      'countries' => $this->exampleCountries,
      'country_code' => 'be',
      'allowed' => TRUE,
    ];

    $cases['unset'] = [
      'countries' => $this->exampleCountries,
      'country_code' => NULL,
      'allowed' => FALSE,
    ];

    $cases['allowed'] = [
      'countries' => $this->exampleCountries,
      'country_code' => 'es',
      'allowed' => FALSE,
    ];

    $cases['obscure'] = [
      'countries' => $this->exampleCountries,
      'country_code' => 'xxy',
      'allowed' => FALSE,
    ];

    return $cases;
  }

  /**
   * Tests getting country languages from the country manager.
   *
   * @param array $countries
   *   The country data as expected by ::setUpCountries().
   * @param array $country_codes
   *   An array of country codes.
   * @param array $langcodes
   *   An array of langcodes.
   * @param array $expected
   *   The expected result.
   * @param string $message
   *   A message for the assertion.
   *
   * @covers ::isLanguageAvailable
   * @dataProvider isLanguageAvailableProvider
   */
  public function testIsLanguageAvailable(array $countries, array $country_codes, array $langcodes, array $expected, string $message): void {
    $this->setUpCountries($countries);
    do {
      $results[] = $this->countryManager
        ->isLanguageAvailable(array_shift($country_codes), array_shift($langcodes));
    } while (!empty($country_codes) && !empty($langcodes));
    $this->assertEquals($expected, $results, $message);
  }

  /**
   * Data provider for testIsLanguageAvailable.
   *
   * @return array
   *   A list of test scenarios.
   */
  public function isLanguageAvailableProvider(): array {

    $country_codes = [
      'gbr', 'us', 'de', 'xx', '', 'be', NULL,
      'be', NULL, 'kr', 'eg', 'ma', 'cn', 'und',
    ];
    $langcodes = [
      'xxy', 'en', 'it', 'yy', '', 'nl', NULL,
      NULL, 'en', 'en', 'en', 'fr', 'zh-hans', 'en',
    ];

    $cases['no-countries'] = [
      'countries' => [],
      'country_codes' => $country_codes,
      'langcodes' => $langcodes,
      'expected' => [
        FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE,
        FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE,
      ],
      'message' => 'All combinations are FALSE when no countries exist.',
    ];

    $cases['countries'] = [
      'countries' => $this->exampleCountries,
      'country_codes' => $country_codes,
      'langcodes' => $langcodes,
      'expected' => [
        FALSE, TRUE, FALSE, FALSE, FALSE, TRUE, FALSE,
        FALSE, FALSE, TRUE, FALSE, TRUE, FALSE, FALSE,
      ],
      'message' => 'Language-country combinations are validated correctly.',
    ];

    return $cases;
  }

  /**
   * Tests getting the primary language for a given country.
   *
   * @param array $countries
   *   The country data as expected by ::setUpCountries().
   * @param array $country_codes
   *   An array of country codes.
   * @param array $expected
   *   The expected result.
   * @param string $message
   *   A message for the assertion.
   *
   * @covers ::getPrimaryLangcode
   * @dataProvider getPrimaryLangcodeProvider
   */
  public function testGetPrimaryLangcode(array $countries, array $country_codes, array $expected, string $message): void {
    $this->setUpCountries($countries);
    do {
      $results[] = $this->countryManager
        ->getPrimaryLangcode(array_shift($country_codes));
    } while (!empty($country_codes));
    $this->assertEquals($expected, $results, $message);
  }

  /**
   * Data provider for testGetPrimaryLangcode.
   *
   * @return array
   *   A list of test scenarios.
   */
  public function getPrimaryLangcodeProvider(): array {

    $country_codes = [
      'de', 'be', 'us', 'kr', 'ma', 'eg',
      'cn', 'xx', 'xxy', NULL, '', 'und',
    ];

    $cases['no-countries'] = [
      'countries' => [],
      'country_codes' => $country_codes,
      'expected' => [
        NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL,
      ],
      'message' => 'Primary language is evaluated correctly when no countries exist.',
    ];

    $cases['countries'] = [
      'countries' => $this->exampleCountries,
      'country_codes' => $country_codes,
      'expected' => [
        'de', 'nl', 'en', 'en', 'fr', NULL,
        NULL, NULL, NULL, NULL, NULL, NULL,
      ],
      'message' => 'Primary language is evaluated correctly.',
    ];

    return $cases;
  }

}
