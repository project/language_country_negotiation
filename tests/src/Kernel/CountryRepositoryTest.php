<?php

declare(strict_types=1);

namespace Drupal\Tests\language_country_negotiation\Kernel;

use Drupal\language_country_negotiation\Service\CountryRepositoryInterface;

/**
 * Tests the language_country_negotiation.country_repository service.
 *
 * @coversDefaultClass \Drupal\language_country_negotiation\Service\CountryRepository
 * @group language_country_negotiation
 */
class CountryRepositoryTest extends CountryTestBase {

  /**
   * The language_country_negotiation.country_repository service.
   *
   * @var \Drupal\language_country_negotiation\Service\CountryRepositoryInterface
   */
  protected CountryRepositoryInterface $countryRepository;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->countryRepository = $this->container->get('language_country_negotiation.country_repository');
  }

  /**
   * Tests getting countries from the country repository.
   *
   * @param array $countries
   *   The country data as expected by ::setUpCountries().
   * @param array $expected
   *   The expected result.
   * @param string $message
   *   A message for the assertion.
   *
   * @covers ::getCountries
   * @dataProvider getCountriesProvider
   */
  public function testGetCountries(array $countries, array $expected, string $message): void {
    $this->setUpCountries($countries);
    $this->assertEquals($expected, $this->countryRepository->getCountries(), $message);
  }

  /**
   * Data provider for testGetCountries.
   *
   * @return array
   *   A list of test scenarios.
   */
  public function getCountriesProvider(): array {

    $cases['no-countries'] = [
      'countries' => [],
      'expected' => [],
      'message' => 'Nothing is returned when no countries exist.',
    ];

    $cases['countries'] = [
      'countries' => $this->exampleCountries,
      'expected' => [
        'de' => 'Germany',
        'be' => 'Belgium',
        'us' => 'United States of America',
        'kr' => 'South Korea',
        'ma' => 'Morocco',
      ],
      'message' => 'Country names are returned when countries exist.',
    ];

    return $cases;
  }

  /**
   * Tests getting country languages from the country repository.
   *
   * @param array $countries
   *   The country data as expected by ::setUpCountries().
   * @param array $expected
   *   The expected result.
   * @param string $message
   *   A message for the assertion.
   *
   * @covers ::getCountryLanguages
   * @dataProvider getCountryLanguagesProvider
   */
  public function testGetCountryLanguages(array $countries, array $expected, string $message): void {
    $this->setUpCountries($countries);
    $this->assertSame($expected, $this->countryRepository->getCountryLanguages(), $message);
  }

  /**
   * Data provider for testGetCountryLanguages.
   *
   * @return array
   *   A list of test scenarios.
   */
  public function getCountryLanguagesProvider(): array {

    $cases['no-countries'] = [
      'countries' => [],
      'expected' => [],
      'message' => 'Nothing is returned when no countries exist.',
    ];

    $cases['countries'] = [
      'countries' => $this->exampleCountries,
      'expected' => [
        'de' => ['de'],
        'be' => ['nl', 'fr', 'de'],
        'us' => ['en'],
        'kr' => ['en'],
        'ma' => ['fr'],
      ],
      'message' => 'Country languages are returned when countries exist.',
    ];

    return $cases;
  }

}
