/**
 * @file
 * Attaches behaviors for country-dependent active link marking.
 */

((Drupal, drupalSettings) => {
  /**
   * Country-dependent 'is-active' class.
   *
   * See misc/active-link, which sets the active link class for each country
   * link. Here, the class is removed for links that do not refer to the current
   * country.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.lcnActiveLinks = {
    attach(context) {
      const countryCode = drupalSettings.currentCountryCode;
      const notActiveLinks = context.querySelectorAll(
        `[data-drupal-link-system-path].is-active [data-country-code]:not([data-country-code=${countryCode}])`,
      );

      const il = notActiveLinks.length;
      for (let i = 0; i < il; i++) {
        const notActiveLinkListItem = notActiveLinks[i].parentElement;
        notActiveLinkListItem.classList.remove('is-active');
        if (!notActiveLinkListItem.classList.length) {
          notActiveLinkListItem.removeAttribute('class');
        }
      }
    },
  };
})(Drupal, drupalSettings);
