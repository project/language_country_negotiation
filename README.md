# Language-Country Negotiation

The Language-Country Negotiation module aims to provide language negotiation functionality in Drupal while also handling the current country of the visitor via a language-country path prefix.

## What does this module do?

With the Language-Country Negotiation module, users can navigate your site using URLs such as:

> `example.com/{$LANGCODE}-{$COUNTRY_CODE}`.

This approach informs the language manager of the current language and the current country service of the visitor's country code. For example:

- example.com/de-de (German in Germany)
- example.com/en-gb (English in Great Britain)
- example.com/en-kr (English in South Korea)

Additionally, users can access the site without a path prefix, entering an _international_ state. In this mode, other negotiation methods such as _URL (Path prefix)_ can come into play. This feature is particularly useful when implementing a country banner or allowing users to browse the website without specifying a country.

## How to use?

1. **Installation:** Start by installing the module via Composer.
2. **Add Languages:**: Go to _Configuration > Region and language > Languages_ and add all required languages.
3. **Add Countries:** Go to _Configuration > Region and language > Countries_ and add countries with their available languages.
4. **Activation:** Go to _Configuration > Region and language > Languages > Detection_ and selection and activate the _Language-country URL_ detection method.
5. **Configuration (Planned):** Customize the detection method with prefix patterns and language codes. Optionally, enable negotiation for admin pages and strict negotiation (disallowing the international state).
6. **Fallbacks:** Navigate to _Configuration > Region and language > Languages > Language-country fallbacks_ to activate convenient fallback methods for the path prefix and the path alias manager.
7. **Extend:** Extend the country entities through fields to use in downstream business logic, e.g., tax label or indexation options.

## Features

This module provides a lightweight implementation of a language negotiation plugin, with the following objectives:

1. **Current Country Service:** Offers a streamlined `CurrentCountry` service that can be consumed by downstream logic, enabling easy retrieval of the current country code. Use as follows: `$this->currentCountry->getCurrentCountryCode();`
2. **Flexible Country Entities:** Allows easy extension via fieldable country entities, which govern all language-country combinations.
3. **Configurable Language Per Country:** Makes it possible to configure allowed languages per country, offering flexibility in content delivery.
4. **Enhanced Path Processing:** Improves the end-user experience with adaptable path processing. The module includes a fallback path alias manager.
5. **Integration (Planned):** Integrates with custom translation fallback chains for different countries based on the _Entity Language Fallback_ module.

## Development Plan

Please note that this module is still in development, and several planned enhancements are in the pipeline before reaching a stable release. These include:

1. ~~Custom Entity for Countries: Implement a custom entity structure for representing countries.~~ (Done)
2. ~~Country Switcher Block: Improve the country switcher block and its corresponding template.~~ (Done)
3. Plugin Configuration: Allow configuration options for the prefix pattern, exclusion of admin pages from negotiation, and enabling strict negotiation (disallowing the international state).
4. Sub-Modules: Create submodules to manage different fallback mechanisms.
5. Integration: Connect with the Entity Language Fallback module.
6. Access Policy Sub-Module: Provide a submodule with access policy features.

## Similar projects

For similar functionality, you may explore the _Domain_ module ecosystem. Additionally, you can achieve similar behavior with the _Domain Country Path_ module.

## Sponsored by Factorial GmbH

This project is sponsored by [Factorial GmbH](https://www.factorial.io). Drop us a line if you are looking for interesting open source employment.

## Maintainer

- Simon Bäse
- Anna Demianik
