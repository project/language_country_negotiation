<?php

namespace Drupal\language_country_negotiation\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\user\EntityOwnerInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the country entity class.
 *
 * @ContentEntityType(
 *   id = "lcn_country",
 *   label = @Translation("Country"),
 *   label_collection = @Translation("Countries"),
 *   label_singular = @Translation("country"),
 *   label_plural = @Translation("countries"),
 *   label_count = @PluralTranslation(
 *     singular = "@count country",
 *     plural = "@count countries"
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\language_country_negotiation\CountryListBuilder",
 *     "form" = {
 *       "default" = "Drupal\language_country_negotiation\Form\CountryForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "lcn_country",
 *   data_table = "lcn_country_field_data",
 *   translatable = TRUE,
 *   fieldable = TRUE,
 *   admin_permission = "administer languages",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "label" = "name",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *     "published" = "status",
 *     "uid" = "uid",
 *     "owner" = "uid"
 *   },
 *   links = {
 *     "add-form" = "/admin/config/regional/country/add",
 *     "edit-form" = "/admin/config/regional/country/{lcn_country}/edit",
 *     "delete-form" = "/admin/config/regional/country/{lcn_country}/delete",
 *     "collection" = "/admin/config/regional/country"
 *   },
 *   field_ui_base_route = "entity.lcn_country.collection"
 * )
 */
class Country extends ContentEntityBase implements EntityChangedInterface, EntityOwnerInterface, EntityPublishedInterface {

  use EntityOwnerTrait;
  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * Gets the country code.
   */
  public function getCountryCode(): string {
    return strtolower($this->get('country_code')->value);
  }

  /**
   * Gets the available languages referenced by the country entity.
   *
   * @return \Drupal\Core\Language\LanguageInterface[]
   *   An array of languages.
   */
  public function getAvailableLanguages(): array {
    $field_languages = $this->get('available_languages');
    if ($field_languages instanceof EntityReferenceFieldItemListInterface) {
      /** @var \Drupal\Core\Language\LanguageInterface[] $languages */
      $languages = $field_languages->referencedEntities();
      return $languages;
    }
    return [];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\Exception\UnsupportedEntityTypeDefinitionException
   *    Thrown when the entity type does not implement respective traits or
   *    if it does not have necessary entity keys.
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::publishedBaseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Country name'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(new TranslatableMarkup('Created'))
      ->setDescription(new TranslatableMarkup('The time that the country was created.'))
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'timestamp',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(new TranslatableMarkup('Changed'))
      ->setDescription(new TranslatableMarkup('The time that the country was last edited.'))
      ->setTranslatable(TRUE);

    $fields['country_code'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Country code'))
      ->setDescription(new TranslatableMarkup('The two-letter code of the country, for example <em>de</em> for Germany.'))
      ->setRequired(TRUE)
      ->setTranslatable(FALSE)
      ->addConstraint('LcnCountryCode')
      ->addConstraint('UniqueField')
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['available_languages'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Available languages'))
      ->setDescription(new TranslatableMarkup('The available languages for the defined country. The order is respected in the language selector.'))
      ->setSetting('target_type', 'configurable_language')
      ->setTranslatable(FALSE)
      ->setRequired(TRUE)
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->addConstraint('ExcludeLockedLanguages')
      ->addConstraint('DistinctReference')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => -3,
        'settings' => [
          'handler' => 'default:configurable_language',
          'match_operator' => 'CONTAINS',
          'match_limit' => 3,
          'size' => 60,
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // @phpstan-ignore-next-line
    $fields[$entity_type->getKey('published')]
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE);

    // @phpstan-ignore-next-line
    $fields[$entity_type->getKey('langcode')]
      ->setDescription(new TranslatableMarkup('The language of this country entity.'));

    return $fields;
  }

  /**
   * Overwrites toUrl method for non-present canonical route.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function toUrl($rel = 'canonical', array $options = []): Url {
    if ($rel === 'canonical') {
      return Url::fromUri('route:<nolink>')->setOptions($options);
    }
    return parent::toUrl($rel, $options);
  }

}
