<?php

namespace Drupal\language_country_negotiation\Service;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Utility\Error;
use Psr\Log\LoggerInterface;

/**
 * Provides a repository of country-language pairs.
 */
class CountryRepository implements CountryRepositoryInterface {

  /**
   * The full country data or FALSE if none was set yet.
   *
   * @var array|null
   */
  protected $countryData;

  /**
   * Constructs a new CountryRepository service object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected LanguageManagerInterface $languageManager,
    protected LoggerInterface $logger,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function getCountries(): array {
    foreach ($this->getCountryData() as $country_code => $data) {
      $list[$country_code] = $data['label'];
    }
    return $list ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getCountryLanguages(): array {
    foreach ($this->getCountryData() as $country_code => $data) {
      $list[$country_code] = $data['languages'];
    }
    return $list ?? [];
  }

  /**
   * Retrieves all the country data.
   *
   * @return array
   *   The country data as an array where the keys are country codes and values
   *   are the label and list of langcodes.
   */
  protected function getCountryData(): array {

    if (isset($this->countryData)) {
      return $this->countryData;
    }

    try {
      $storage = $this->entityTypeManager
        ->getStorage('lcn_country');
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      $variables = Error::decodeException($e);
      $this->logger->error($e->getMessage(), $variables);
      return $this->countryData = [];
    }

    $country_ids = $storage->getQuery()
      ->accessCheck(FALSE)
      ->exists('country_code')
      ->condition('status', TRUE)
      ->execute();

    if (empty($country_ids)) {
      return $this->countryData = [];
    }

    $current_langcode = $this->languageManager
      ->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();

    /** @var \Drupal\language_country_negotiation\Entity\Country $country */
    foreach ($storage->loadMultiple($country_ids) as $country) {

      if ($country_code = $country->getCountryCode()) {

        if ($country->hasTranslation($current_langcode)) {
          $country = $country->getTranslation($current_langcode);
        }

        $this->countryData[$country_code]['label'] = $country->label();

        $this->countryData[$country_code]['languages'] = [];
        foreach ($country->getAvailableLanguages() as $language) {
          $this->countryData[$country_code]['languages'][] = $language->getId();
        }
      }
    }

    return $this->countryData;
  }

}
