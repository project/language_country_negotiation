<?php

namespace Drupal\language_country_negotiation\Service;

/**
 * Interface for a country-language repository.
 */
interface CountryRepositoryInterface {

  /**
   * Gets all the configured countries.
   *
   * @return array
   *   A list of available country names, keyed per country code.
   */
  public function getCountries(): array;

  /**
   * Gets all the configured languages by country.
   *
   * @return array[]
   *   A list of available language codes, keyed per country code.
   */
  public function getCountryLanguages(): array;

}
