<?php

namespace Drupal\language_country_negotiation\Plugin\Validation\Constraint;

use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Language\LanguageInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates that a reference field excludes locked languages.
 */
class ExcludeLockedLanguagesConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate(mixed $value, Constraint $constraint): void {

    if (!$value instanceof EntityReferenceFieldItemListInterface) {
      return;
    }

    $locked_languages = array_filter($value->referencedEntities(),
      static fn($l) => $l instanceof LanguageInterface && $l->isLocked());

    if (count($locked_languages)) {
      /** @var \Drupal\language_country_negotiation\Plugin\Validation\Constraint\ExcludeLockedLanguagesConstraint $constraint */
      $this->context->addViolation($constraint->message, [
        '@field_name' => strtolower($value->getFieldDefinition()->getLabel()),
      ]);
    }
  }

}
