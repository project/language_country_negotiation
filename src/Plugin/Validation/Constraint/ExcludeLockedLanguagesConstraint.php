<?php

namespace Drupal\language_country_negotiation\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks if a reference field excludes locked languages.
 *
 * @Constraint(
 *   id = "ExcludeLockedLanguages",
 *   label = @Translation("Exclude locked languages constraint", context = "Validation"),
 * )
 */
class ExcludeLockedLanguagesConstraint extends Constraint {

  /**
   * The constraint message.
   *
   * @var string
   */
  public string $message = 'The @field_name field may not reference locked languages, such as <em>Not specified</em> or <em>Not applicable</em>.';

}
