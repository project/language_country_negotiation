<?php

namespace Drupal\language_country_negotiation\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks if a country code field provides a two-letter value.
 *
 * @Constraint(
 *   id = "LcnCountryCode",
 *   label = @Translation("Country code constraint", context = "Validation"),
 * )
 */
class CountryCodeConstraint extends Constraint {

  /**
   * The constraint message.
   *
   * @var string
   */
  public string $message = 'The @field_name %value is not a two-letter value.';

}
