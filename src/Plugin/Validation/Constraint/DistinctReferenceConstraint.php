<?php

namespace Drupal\language_country_negotiation\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks if a reference field has distinct values.
 *
 * @Constraint(
 *   id = "DistinctReference",
 *   label = @Translation("Distinct reference constraint", context = "Validation"),
 * )
 */
class DistinctReferenceConstraint extends Constraint {

  /**
   * The constraint message.
   *
   * @var string
   */
  public string $message = 'The @field_name field needs to provide distinct references.';

}
