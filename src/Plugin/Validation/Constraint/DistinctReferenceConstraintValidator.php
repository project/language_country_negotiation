<?php

namespace Drupal\language_country_negotiation\Plugin\Validation\Constraint;

use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates that a reference field has distinct values.
 */
class DistinctReferenceConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate(mixed $value, Constraint $constraint): void {

    if (!$value instanceof EntityReferenceFieldItemListInterface) {
      return;
    }

    $ids = array_map(static fn($r) => $r->id(), $value->referencedEntities());

    if (count($ids) !== count(array_unique($ids))) {
      /** @var \Drupal\language_country_negotiation\Plugin\Validation\Constraint\DistinctReferenceConstraint $constraint */
      $this->context->addViolation($constraint->message, [
        '@field_name' => strtolower($value->getFieldDefinition()->getLabel()),
      ]);
    }
  }

}
