<?php

namespace Drupal\language_country_negotiation\Plugin\Validation\Constraint;

use Drupal\Core\Field\FieldItemListInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates that a country code field provides a two-letter value.
 */
class CountryCodeConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate(mixed $value, Constraint $constraint): void {

    if (!$value instanceof FieldItemListInterface) {
      return;
    }

    $country_code = $value->value;

    if (!is_string($country_code) || strlen($country_code) !== 2) {
      /** @var \Drupal\language_country_negotiation\Plugin\Validation\Constraint\CountryCodeConstraint $constraint */
      $this->context->addViolation($constraint->message, [
        '%value' => $country_code,
        '@field_name' => strtolower($value->getFieldDefinition()->getLabel()),
      ]);
    }
  }

}
