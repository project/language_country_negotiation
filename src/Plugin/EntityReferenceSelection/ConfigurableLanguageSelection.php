<?php

namespace Drupal\language_country_negotiation\Plugin\EntityReferenceSelection;

use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;
use Drupal\Core\Entity\Query\QueryInterface;

/**
 * Provides specific access control for the configurable language entity type.
 *
 * @EntityReferenceSelection(
 *    id = "default:configurable_language",
 *    label = @Translation("Configurable language"),
 *    entity_types = {"configurable_language"},
 *    group = "default",
 *    weight = 1
 *  )
 */
class ConfigurableLanguageSelection extends DefaultSelection {

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\Exception\UnsupportedEntityTypeDefinitionException
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS'): QueryInterface {

    $query = parent::buildEntityQuery($match, $match_operator);
    $query->condition('locked', FALSE);
    return $query;
  }

}
