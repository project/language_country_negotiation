<?php

namespace Drupal\language_country_negotiation\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\language_country_negotiation\Service\CountryManagerInterface;
use Drupal\language_country_negotiation\Service\CountryRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a country switcher block.
 *
 * @Block(
 *   id = "language_country_negotiation_country_switcher",
 *   admin_label= @Translation("Country switcher")
 * )
 */
class CountrySwitcherBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The country manager.
   *
   * @var \Drupal\language_country_negotiation\Service\CountryManagerInterface
   */
  protected CountryManagerInterface $countryManager;

  /**
   * The country repository.
   *
   * @var \Drupal\language_country_negotiation\Service\CountryRepositoryInterface
   */
  protected CountryRepositoryInterface $countryRepository;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * The path matcher.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected PathMatcherInterface $pathMatcher;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, ...$defaults): static {
    $instance = new static(...$defaults);
    $instance->countryManager = $container->get('language_country_negotiation.country_manager');
    $instance->countryRepository = $container->get('language_country_negotiation.country_repository');
    $instance->languageManager = $container->get('language_manager');
    $instance->pathMatcher = $container->get('path.matcher');
    $instance->requestStack = $container->get('request_stack');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {

    $current_url = $this->pathMatcher->isFrontPage() ? Url::fromRoute('<front>') : Url::fromRoute('<current>');
    $query = $this->requestStack->getCurrentRequest()->query->all();

    $current_langcode = $this->languageManager->getCurrentLanguage(LanguageInterface::TYPE_URL)->getId();
    $native_languages = $this->languageManager->getNativeLanguages();

    $current_country_code = $this->countryManager->getCurrentCountryCode();
    $countries = $this->countryRepository->getCountries();

    // Maybe move to negotiation plugin. Build list of country links.
    $links = [];
    foreach ($countries as $country_code => $country_name) {

      // Resolve target langcode. Either the current language is allowed in
      // the target country or the primary language in the target is used.
      if ($this->countryManager->isLanguageAvailable($country_code, $current_langcode)) {
        $langcode = $current_langcode;
      }
      else {
        $langcode = $this->countryManager->getPrimaryLangcode($country_code);
      }

      // Select the target language object from the native languages.
      if (!$language = $native_languages[$langcode] ?? NULL) {
        continue;
      }

      // Resolve the target URL for this country by passing the country code
      // as an option. The country code option is resolved in the outbound
      // path processing of the language country negotiation plugin.
      $url = clone $current_url;
      $url->setOptions([
        'language' => $language,
        'country_code' => $country_code,
        'query' => $query,
      ]);

      // Generate country url based on current route.
      $links[] = [
        'url' => $url,
        'title' => $country_name,
        'country_code' => $country_code,
        'attributes' => [
          'class' => 'country-link',
          'data-country-code' => $country_code,
        ],
        'is_active' => $country_code === $current_country_code,
        'query' => $query,
      ];
    }

    return [
      '#theme' => 'links__country_switcher_block',
      '#links' => $links,
      '#set_active_class' => TRUE,
      '#attached' => [
        'library' => ['language_country_negotiation/active-link'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts(): array {
    return Cache::mergeContexts(parent::getCacheContexts(), [
      'url.query_args',
      'url.path',
      'current_country',
    ]);
  }

}
