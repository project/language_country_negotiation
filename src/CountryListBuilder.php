<?php

namespace Drupal\language_country_negotiation;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class to build a listing of country entities.
 *
 * @see \Drupal\language_country_negotiation\Entity\Country
 */
class CountryListBuilder extends EntityListBuilder {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type): static {
    $instance = parent::createInstance($container, $entity_type);
    $instance->dateFormatter = $container->get('date.formatter');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header = [
      'name' => $this->t('Country name'),
      'country_code' => [
        'data' => $this->t('Country code'),
        'class' => [RESPONSIVE_PRIORITY_MEDIUM],
      ],
      'status' => $this->t('Status'),
      'changed' => [
        'data' => $this->t('Updated'),
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
    ];
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {

    /** @var \Drupal\language_country_negotiation\Entity\Country $entity */
    $row['title']['data'] = $entity->label();
    $row['country_code'] = $entity->getCountryCode();
    $row['status'] = $entity->isPublished() ?
      $this->t('published') : $this->t('not published');
    $row['changed'] = $this->dateFormatter->format($entity->getChangedTime(), 'short');

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render(): array {
    $build = parent::render();
    $build['table']['#empty'] = $this->t('There are no countries yet. <a href=":link">Add country</a>.', [
      ':link' => $this->ensureDestination(Url::fromRoute('entity.lcn_country.add_form'))->toString(),
    ]);
    return $build;
  }

}
