<?php

namespace Drupal\language_country_negotiation\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form controller for country entities.
 *
 * @internal
 */
class CountryForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function save(array $form, FormStateInterface $form_state): int {

    $result = parent::save($form, $form_state);
    $edit_link = $this->entity->toLink($this->t('Edit'), 'edit-form')->toString();

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()
          ->addStatus($this->t('Created new country %country.',
            ['%country' => $this->entity->label()]));
        $this->logger('language_country_negotiation')
          ->notice('Created new country %country.',
            ['%country' => $this->entity->label(), 'link' => $edit_link]);
        break;

      case SAVED_UPDATED:
        $this->messenger()
          ->addStatus($this->t('Updated country %country.',
            ['%country' => $this->entity->label()]));
        $this->logger('language_country_negotiation')
          ->notice('Updated country %country.',
            ['%country' => $this->entity->label(), 'link' => $edit_link]);
        break;
    }

    $form_state->setRedirect('entity.lcn_country.collection');

    return $result;
  }

}
