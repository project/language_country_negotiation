<?php

namespace Drupal\language_country_negotiation\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\language_country_negotiation\Service\CountryRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure the URL language-country negotiation method for this site.
 *
 * @internal
 */
class NegotiationLanguageCountryUrlForm extends ConfigFormBase {

  /**
   * The country repository.
   *
   * @var \Drupal\language_country_negotiation\Service\CountryRepositoryInterface
   */
  protected CountryRepositoryInterface $countryRepository;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    $instance = parent::create($container);
    $instance->countryRepository = $container->get('language_country_negotiation.country_repository');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'language_country_negotiation_configure_url_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['language_country_negotiation.negotiation'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $config = $this->config('language_country_negotiation.negotiation');

    $form['prefix_pattern'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Prefix pattern'),
      '#description' => $this->t('Not used, yet.'),
      '#default_value' => '',
      '#disabled' => TRUE,
    ];

    $form['prefixes'] = [
      '#type' => 'details',
      '#tree' => TRUE,
      '#title' => $this->t('Language-country path prefixes'),
      '#open' => TRUE,
      '#description' => $this->t('The path prefixes are computed based on the provided countries, taking into account the combination of the country code and the specific language codes allowed for each individual country. The following lists shows the computed prefixes for each country. Please <a href=":link">edit the countries</a> to adjust the allowed language-country combinations.', [':link' => Url::fromRoute('entity.lcn_country.collection')->toString()]),
    ];

    $countries = $this->countryRepository->getCountries();
    $country_languages = $this->countryRepository->getCountryLanguages();

    foreach ($country_languages as $country_code => $languages) {
      $prefixes = [];
      foreach ($languages as $langcode) {
        $prefixes[] = $langcode . '-' . $country_code;
      }
      $form['prefixes'][$country_code] = [
        '#type' => 'markup',
        '#markup' => '<h6>' . $countries[$country_code] . '</h6><p>' . implode(', ', $prefixes) . '</p>',
      ];
    }

    $form['exclude_admin_pages'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Exclude admin pages'),
      '#description' => $this->t('Enable this option to exclude all admin pages from language-country negotiation, helping to minimize unnecessary dynamic page cache variations. When enabled, a subsequent language negotiator (e.g., URL-based) should be used to ensure proper path processing.'),
      '#default_value' => $config->get('exclude_admin_pages') ?? TRUE,
    ];

    $form['strict_negotiation'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Strict negotiation'),
      '#description' => $this->t('Activate to enable strict negotiation which does not allow an international state (without country). Not used, yet.'),
      '#default_value' => $config->get('strict_negotiation') ?? FALSE,
      '#disabled' => TRUE,
    ];

    $form_state->setRedirect('language.negotiation');

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {

    $this->config('language_country_negotiation.negotiation')
      ->set('prefix_pattern', $form_state->getValue('prefix_pattern'))
      ->set('exclude_admin_pages', $form_state->getValue('exclude_admin_pages'))
      ->set('strict_negotiation', $form_state->getValue('strict_negotiation'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
