<?php

namespace Drupal\language_country_negotiation\Cache\Context;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\language_country_negotiation\Service\CurrentCountryInterface;

/**
 * Defines the CountryCacheContext service, for "per country" caching.
 */
class CountryCacheContext implements CacheContextInterface {

  /**
   * Constructs a new CountryCacheContext object.
   *
   * @param \Drupal\language_country_negotiation\Service\CurrentCountryInterface $currentCountry
   *   The current country service.
   */
  public function __construct(
    protected CurrentCountryInterface $currentCountry,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function getLabel(): string {
    return t('Country');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext(): string {
    return $this->currentCountry->getCurrentCountryCode() ?? 'int';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata(): CacheableMetadata {
    return new CacheableMetadata();
  }

}
