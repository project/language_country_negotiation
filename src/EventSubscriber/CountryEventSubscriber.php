<?php

namespace Drupal\language_country_negotiation\EventSubscriber;

use Drupal\language_country_negotiation\PathUtility;
use Drupal\language_country_negotiation\Service\CountryManagerInterface;
use Drupal\language_country_negotiation\Service\CurrentCountryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Sets the current country based on the language-country prefix.
 *
 * The current country in the current country service is set, if the resolved
 * country is allowed. This can be understood as an initialization for the
 * current country service. This happens early in the request handling because
 * other services, language negotiation plugins and alias management depend on
 * the current country.
 *
 * This should be the only place where the current country is set.
 */
class CountryEventSubscriber implements EventSubscriberInterface {

  /**
   * Constructs a new CountryEventSubscriber object.
   *
   * @param \Drupal\language_country_negotiation\Service\CountryManagerInterface $countryManager
   *   The country manager.
   * @param \Drupal\language_country_negotiation\Service\CurrentCountryInterface $currentCountry
   *   The current country service.
   */
  public function __construct(
    protected CountryManagerInterface $countryManager,
    protected CurrentCountryInterface $currentCountry,
  ) {}

  /**
   * Sets a valid current country code in the current country service.
   */
  public function setCountryCode(RequestEvent $event): void {

    if (!$event->isMainRequest()) {
      return;
    }

    $path = $event->getRequest()->getPathInfo();
    $country_code = PathUtility::getCountryCodeFromPath($path);
    if ($this->countryManager->isCountryAllowed($country_code)) {
      $this->currentCountry->setCountryCode($country_code);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      KernelEvents::REQUEST => ['setCountryCode', 305],
    ];
  }

}
